import java.util.LinkedList;
import java.util.List;

import interfaces.IDQueue;
import objects.DLine;
import objects.DQueue;

/**
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * -> Class: Data Structures - 2720 - - - - - - - - - - - - - - - - - - - - - -
 * -> LAB: 10 - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * -> Date: Friday 26 Oct, 2018 - - - - - - - - - - - - - - - - - - - - - - - -
 * -> Subject: Queue- - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * -> Lab Web-page: - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * [https://sites.google.com/view/azimahmadzadeh/teaching/data-structures-2720]
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 * 
 * @author Azim Ahmadzadeh [https://grid.cs.gsu.edu/~aahmadzadeh1/] - - - - -
 */
public class Main {

	public static void main(String[] args) {

			
		IDQueue<DLine> myQ1 = new DQueue();
		
		myQ1.displayQueue();
		for(int i = 1; i <= 10; i++) {
			myQ1.enqueue(new DLine(i * 10));
			myQ1.displayQueue();
		}
		
		for(int i = 1; i <= 10+2; i++) {
			myQ1.dequeue();
			myQ1.displayQueue();
		}
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
}
